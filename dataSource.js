// Mongoose impl
var mongoose = require("mongoose");
var schema = require('./lab4files/lab4schema.js');
var dbAddress = "mongodb://localhost/e422match";

var Question = mongoose.model('questions', schema.questionsSchema);
var Response = mongoose.model('responses', schema.responsesSchema);

// Used to provide quick access to static question data
// I decided to cache this as questions should change very
// rarely, if ever and I make lots of calls to this data via getQuestionLength()
var questionsCache;

var options = {
    keepAlive : 1
};
mongoose.connect(dbAddress, options, function(err) {
    if (!err) {
        updateQuestionsCache();
        console.log("OK! Connected to MongoDB");
    } else {
        console.log("error connecting to database");
        console.log(err);
    }
});

function updateQuestionsCache() {
    Question.find({}, function (err, docs) {
        questionsCache = docs;
    });
}

function getQuestion(questionNumber) {
    return questionsCache[questionNumber];
}

function getQuestionsLength() {
    var returnable = questionsCache;
    updateQuestionsCache();
    return returnable.length;
}

function deleteIfIncomplete(uName) {

    Response.findOne({name: uName}, function (err, docs) {
       if (docs) {
           var bolIncomplete = false;
           for (var i = 0; i < docs.responses.length; i++) {
               if (parseInt(docs.responses[i]) == -1) bolIncomplete = true;
           }

           if (bolIncomplete == true) {
               Response.remove({name: uName}, function (err) {
                  if (err) {
                      console.log("unable to remove on timer: " + uName);
                  } else {
                      console.log("Automatic incomplete response removal success for: " + uName);
                  }
               });
           }
       }
    });

}

// Expose schema objects for some nice tight coupling!
module.exports.Question = Question;
module.exports.Response = Response;

// Stuff left over from old implementation
module.exports.getQuestion = getQuestion;
module.exports.getQuestionsLength = getQuestionsLength;
module.exports.deleteIfIncomplete = deleteIfIncomplete;
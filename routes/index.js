var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

    // Get user name from cookies or from query string
    var uName;
    if (req.query.uName) {
        uName = req.query.uName;
    } else if (req.cookies.uName) {
        uName = req.cookies.uName;
    }
    res.render('index', { userName: uName});
});

module.exports = router;

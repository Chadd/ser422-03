var express = require('express');
var router = express.Router();
var data = require('../dataSource.js');

router.get('/', function(req, res, next) {
    var qID;
    var uName;

    // Determine if this is a new session and if so. set it up
    if (req.query.uName) {
        res.cookie('uName', req.query.uName, {path: '/', secure: false });
        uName = req.query.uName;
    } else {
        uName = req.cookies.uName;
    }

    if (req.query.qID) {
        qID = parseInt(req.query.qID);
    } else {
        qID = 0;
    }

    // If we have a user name, check to see if they have existing responses
    // if they do not, we will create a new entry and set a timer for it to expire
    data.Response.findOne({name: uName}, function (err, docs) {

        if (!docs) {
            var emptyResponses = [];
            var qCount = data.getQuestionsLength();
            for (var i = 0; i < qCount; i++) {
                emptyResponses[i] = -1;
            }

            data.Response.create({name: uName, gpa: "0.00", responses: emptyResponses},
                function (err) {
                    if (err) console.log("Could not add new user: " + uName);
                }
            );

            // Set timer to self destruct if not completed in 30 seconds
            setTimeout(function() {
                data.deleteIfIncomplete(uName);
            },30000);
        }

    });

    if (req.query.results) {
        res.redirect('/quiz/results?uName=' + uName);
    } else {
        res.render('question', { qID: qID, question: data.getQuestion(qID)});
    }

});

router.get('/results', function(req, res, next) {

    var uName;
    if (req.query.uName) {
        uName = req.query.uName;
    } else if (req.cookies.uName) {
        uName = req.cookies.uName;
    }

    // Get results for comparison
    data.Response.find({}, function (err, docs) {
        if (!err) {
            var matches = {};
            // Find our result among the docs
            var myResult;
            for (var aid = 0; aid < docs.length; aid++) {
                if (docs[aid].name == uName) {
                    myResult = docs[aid];
                }
            }

            for (var resID = 0; resID < docs.length; resID++) {

                // Skip our own name
                if (docs[resID] != myResult) {
                    var matchScore = 0;
                    for (var i = 0; i < docs[resID].responses.length; i++) {
                        if (docs[resID].responses[i] == myResult.responses[i]) matchScore++;
                    }
                    matches[docs[resID].name] = matchScore;
                }

            }

            // Stolen from; http://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value
            var sortedMatches = Object.keys(matches).sort(
                function(a,b) {
                    if (matches[a] > matches[b]) {
                        return -1;
                    } else if (matches[a] < matches[b]) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            );
            res.render('results', { matches: sortedMatches});

        } else {
            res.render('error', {message: err});
        }

    });

});

router.post('/', function(req, res, next) {

    // Fields needed for a successful post
    var qID = req.body.qID;
    var answerID = req.body.answerID;
    var uName = req.body.uName;

    // If there was no uName in the payload, get it from cookie
    if (!uName) {
        uName = req.cookies.uName;
    }

    // Perform save if all fields are populated
    if (qID && answerID && uName) {

        data.Response.findOne({name: uName}, function (err, docs) {

            if (docs) {
                var aResponse = docs;
                aResponse.responses[qID] = answerID;
                data.Response.update({name: aResponse.name}, {responses: aResponse.responses},
                function (err) {
                    if (err) {
                        console.log("Unable to update response document");
                        console.log(err.toString());
                    }
                });

                // Send to next question, or render completion page
                // Assign 4: Prompt for gpa before finishing
                if (parseInt(qID) < data.getQuestionsLength() - 1) {
                    var nextqID = parseInt(qID) + 1;
                    res.redirect("quiz?qID=" + nextqID);
                } else {
                    res.render('gpa');
                }
            } else {
                res.render('error', {message: "I'm sorry, but it appears your time has expired"});
            }

        });


    } else if (uName && req.body.gpa) {

        // User has submitted their GPA! lets attempt to add it to their record!
        data.Response.findOne({name: uName},
            function(err, myResponse) {
                if (!err) {

                    // Get a reference to the object and update it
                    var newResponse = myResponse;
                    newResponse.gpa = req.body.gpa;

                    // Attempt save and handle validation errors
                    newResponse.save(
                        function(err) {
                            if (!err) {
                                res.render('finish');
                            } else {
                                console.log("Unable to commit GPA for user: " + uName + " with value:" + req.body.gpa);
                                console.log(err.toString());
                                res.render('gpa', {message: "The GPA provided does not appear to be valid, please provide a value between 0.00 and 4.50"});
                            }
                        }
                    );
                } else {
                    console.log("Unable to commit GPA for user: " + uName + " with value:" + req.body.gpa);
                    console.log(err.toString());
                    res.render('gpa', {message: "The GPA provided does not appear to be valid, please provide a value between 0.00 and 4.50"});
                }
            }
        );

    } else {
        res.status(500);
        res.render('error', {message: "There was a problem fulfilling your request; fields missing"});
    }

});

module.exports = router;

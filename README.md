# Assignment 4
Chadd Ingersoll

### Schema and init script
Initialization and scheme files are in the lab4files folder

Deploy the needed collections and documents by running;
> node lab4files/lab4init.js

The lab4init script will exit with code 0 if successful

### Running the app
From the root project directory
> node bin/www

### Notes
* Validation can be found in the lab4schema.js file
* Thank you!
#!/usr/bin/env node

var dbAddress = "mongodb://localhost/e422match";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var schemas = require('./lab4schema.js');

var Question = mongoose.model('questions', schemas.questionsSchema);
var Response = mongoose.model('responses', schemas.responsesSchema);

var initQuestions = [
    {title: "Shall we play a game?", answers:
        [
            "Yes; Chess",
            "Yes; Checkers",
            "Tes; Thermonuclear global war",
            "No; The only winning move, is not to play"
        ]
    },
    {title: "Favorite text editor", answers:
        [
            "Vi or Vim",
            "Emacs",
            "Nano",
            "Ed",
            "Other"
        ]
    },
    {title: "When do you start a programming assignment?", answers:
        [
            "The day it is assigned",
            "The first Saturday after it is assigned",
            "48 hours before it is due",
            "Less than 48 hours before it is due"
        ]
    },
    {title: "Favorite Apple II game?", answers:
        [
            "Oregon Trail",
            "Logo",
            "Lode Runner",
            "Other or None"
        ]
    }
];

var initResponses = [
    {name: "Dilbert", responses: [0,1,2,0], gpa: "4.00"},
    {name: "Wally", responses: [3,2,3,3], gpa: "3.11"},
    {name: "Alice", responses: [2,2,2,3], gpa: "4.15"},
    {name: "Dogbert", responses: [3,3,2,1], gpa: "4.50"},
    {name: "Ratbert", responses: [0,0,0,0], gpa: "2.20"},
    {name: "Loud Howard", responses: [1,1,1,3], gpa: "3.01"}
];

// flow control for exiting
var responsesSaved = false;
var questionsSaved = false;
function initDone() {
    if (responsesSaved && questionsSaved) {
        console.log("Documents appear to have been added");
        process.exit(0);
    }
}

mongoose.connect(dbAddress, function(err) {
    if (!err) {
        Question.create(initQuestions,
            function(err) {
                if (err) {
                    console.log("error saving questions");
                    console.log(err);
                    process.exit(1);
                } else {
                    questionsSaved = true;
                    initDone();
                }
            }
        );

        Response.create(initResponses,
            function(err) {
                if (err) {
                    console.log("error saving responses");
                    console.log(err);
                    process.exit(1);
                } else {
                    responsesSaved = true;
                    initDone();
                }
            }
        );

    } else {
        console.log("error connecting to database");
        console.log(err);
        process.exit(1);
    }


});

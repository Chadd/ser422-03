var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// Validator decleration
function validateGPA(val) {
    var valid = true;

    // Check that passed value is a number if firm #.##
    if (!/^\d(\.\d{2})$/.test(val)) valid = false;

    // convert to number and do min/max checks
    var aNumber = Number(val);
    if (val < 0 || val > 4.5) valid = false;

    return valid;
}

var questionsSchema = new Schema({
    title: String,
    answers: [String]
});
var responsesSchema = new Schema({
    name: String,
    responses: [Number],
    gpa: {type: String, validate: validateGPA}
});

module.exports.questionsSchema = questionsSchema;
module.exports.responsesSchema = responsesSchema;